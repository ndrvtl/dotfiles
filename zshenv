#!/bin/zsh -xe

[[ -z $HOSTNAME ]] && HOSTNAME=$HOST

if [[ -d "/data/data/com.termux/" ]]; then
    export PATH="/data/data/com.termux/files/usr/bin"
else
    export PATH="/usr/bin:/bin:/usr/games:/sbin:/usr/sbin:/usr/local/bin"
fi

# add home .local/bin as per Systemd compatibility
[[ -d "$HOME/.local/bin" ]] && export PATH="$HOME/.local/bin:$PATH"

# add Emacs local build
# [[ -d "/opt/emacs25.2/bin" ]] && export PATH="/opt/emacs25.2/bin:$PATH"

# add haskell cabal to PATH
[[ -d "$HOME/.cabal/bin" ]] && export PATH="$HOME/.cabal/bin:$PATH"

# add rust cargo to PATH
[[ -d "$HOME/.cargo/bin" ]] && export PATH="$HOME/.cargo/bin:$PATH"

# add flatpak bin to PATH
[[ -d /var/lib/flatpak/exports/bin ]] && export PATH="/var/lib/flatpak/exports/bin:$PATH"

# add sbcl common lisp to PATH
if [[ -d "/usr/local/lib/sbcl" ]]; then
    #export PATH="$HOME/.local/share/sbcl/bin:$PATH"
    export SBCL_HOME="/usr/local/lib/sbcl"
fi

# add node npm to PATH
if [[ -d "$HOME/.node_modules_global/bin" ]]; then
    export PATH="$HOME/.node_modules_global/bin:$PATH"
    export NODE_PATH="$HOME/.node_modules_global/lib/node_modules:$NODE_PATH"
fi

export DOCKER_HOST=unix:///run/user/1000/docker.sock

return 0
