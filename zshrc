# -*- mode: sh; sh-shell: zsh -*-
zmodload zsh/regex
autoload -Uz colors add-zsh-hook regex-match zsh/sched zsh/terminfo

colors

# recursively grab previous n-arguments in a line
#autoload -Uz narrow-to-region
autoload -Uz copy-earlier-word
zle -N copy-earlier-word
bindkey '^[,' copy-earlier-word

# background notify
if (( $+commands[notify-send] )); then
    source ~/.dotfiles/bgnotify.zsh
fi

# cycling directories
source ~/.dotfiles/dircycle.zsh

# fd
if (( $+commands[fdfind] )); then
    alias fd="fdfind"
    FZF_CTRL_T_COMMAND="command fdfind --follow ."
    FZF_CTRL_C_COMMAND="command fdfind --follow --type directory ."
fi

# fuzzy find
if (( $+commands[fzf] )); then
    if [[ -f "/usr/share/zsh/vendor-completions/_fzf" ]]; then
        source /usr/share/zsh/vendor-completions/_fzf
    fi
    source ~/.dotfiles/fzf-key-bindings.zsh
    _gen_fzf_default_opts() {
        local color00='#2d2d2d'
        local color01='#393939'
        local color02='#515151'
        local color03='#747369'
        local color04='#a09f93'
        local color05='#d3d0c8'
        local color06='#e8e6df'
        local color07='#f2f0ec'
        local color08='#f2777a'
        local color09='#f99157'
        local color0A='#ffcc66'
        local color0B='#99cc99'
        local color0C='#66cccc'
        local color0D='#6699cc'
        local color0E='#cc99cc'
        local color0F='#d27b53'

        export FZF_DEFAULT_OPTS="
        --color=bg+:$color01,bg:$color00,spinner:$color0C,hl:$color0D
        --color=fg:$color04,header:$color0D,info:$color0A,pointer:$color0C
        --color=marker:$color0C,fg+:$color06,prompt:$color0A,hl+:$color0D
        --height 40% --layout=reverse"
    }
    _gen_fzf_default_opts
fi

# wormhole-william
if (( $+commands[wormhole-william] )); then
    if [[ ! -f "$HOME/.dotfiles/zsh_completion/_wormhole-william" ]]; then
        wormhole-william shell-completion zsh > "$HOME/.dotfiles/zsh_completion/_wormhole-william"
    fi
fi

# enable xz to use all cores/threads
export XZ_OPT="--threads=0"

COLOR_THEME=~/.dotfiles/base16-eighties.sh
#[[ -f "$COLOR_THEME" ]] && [[ $(tty) == "/dev/pts/"*  ]] && eval "$COLOR_THEME"
unset COLOR_THEME

export LS_OPTIONS="--color=auto --classify"
eval $(dircolors ~/.dir_colors)
export LS_COLORS="${LS_COLORS%:}"

export PAGER=less
export LESS="-giMSRX"
# export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
# export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
# export LESS_TERMCAP_me=$'\E[0m'           # end mode
# export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
# export LESS_TERMCAP_so=$'\E[38;5;016m\E[48;5;220m'    # begin standout-mode - info box
# export LESS_TERMCAP_ue=$'\E[0m'           # end underline
# export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

# use ranger with Emacs bindings
export RANGER_LOAD_DEFAULT_RC=FALSE

unset EDITOR
for available in emacsclient nano vim vi ; do
    (( $+commands[$available] )) || continue
    break
done

if [[ "$available" == "emacsclient" ]]; then
    export EDITOR="emacsclient -nw"
    export VISUAL="emacsclient"
    # export ALTERNATE_EDITOR="emacs"
    alias edit="emacsclient -nw 2> /dev/null"
    alias ec="emacsclient 2> /dev/null"
else
    export EDITOR=$available
fi

# globally disable flow control/Ctrl-s
if [[ -o interactive ]]; then
    stty -ixon
fi

#### Various preferences
WORDCHARS="*?_-[]~=&;.!#$%^(){}<>"

## History settings
HISTSIZE=50000
SAVEHIST=50000
HISTFILE=~/.zsh_history
setopt append_history
setopt inc_append_history
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_save_no_dups
setopt share_history

## cd settings
setopt autocd
setopt pushd_silent # omit printing directory stack
setopt auto_pushd   # make cd push directories onto stack
setopt pushd_minus  # invert meanings of +n and -n arguments to pushd
setopt pushd_ignore_dups  # remove duplicate entries
setopt auto_remove_slash

## Shell options
setopt interactive_comments
setopt nohup
setopt printeightbit
setopt extended_glob
unsetopt flowcontrol # this one disable ctrl-s (stop), to enable history-incremental-search-forward
setopt globstarshort

# lesspipe
if (( $+commands[lesspipe] )); then
    eval $(lesspipe)
fi


alias cp="cp -i"
alias mv="mv -i"
alias grep="grep -i --color=auto"
if (( $+commands[rg] )); then
    alias rg="rg -i"
else
    print "\e[31mMissing rg, fallback to grep\!\e[0m"
    alias rg="grep"
fi
if (( $+commands[yank-cli] )); then
    if (( $+commands[wl-copy] )); then
        alias y="/usr/bin/yank-cli -- /usr/bin/wl-copy"
    elif (( $+commands[xsel])); then
        alias y="(){/usr/bin/yank-cli $@ -- /usr/bin/xsel -b ;}"
    else
        alias y="/usr/bin/yank-cli"
   fi
fi
alias nl="nl -ba"
#alias nohistory="export HISTFILE=/dev/null"
alias nohistory="unset HISTFILE"
alias tree="tree -C"
alias noblinklegacy="echo -e '\033[?17;0;40c'"
alias noblink="echo 0 | sudo tee /sys/class/graphics/fbcon/cursor_blink"
alias cursor="echo -e '\033[?25h' | sudo tee /dev/tty0"
alias dmesg="dmesg --color --reltime"
alias acs="apt-cache search"
alias acss="apt-cache show"
alias aps="aptitude search"
alias apss="aptitude show"
alias rlsbcl="rlwrap sbcl"
# alias edit="emacsclient ${$@}&"
alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"
alias clip="xsel -i --clipboard"
alias emacs="GTK_THEME=\"Adwaita:dark\" /usr/bin/emacsclient -c -a \"/usr/bin/emacs\" 2> /dev/null"

alias ls="ls $LS_OPTIONS --hyperlink=auto"
alias ll="ls $LS_OPTIONS -l --hyperlink=auto"
alias la="ls $LS_OPTIONS -lA --hyperlink=auto"
alias lr="ls $LS_OPTIONS -ltr --hyperlink=auto"

alias -g NF="*(.om[1])"
alias -g ND="*(/om[1])"
alias -g NN="*(om[1])"

alias ip="ip -c"

activate() {
    if [[ -f "./venv/bin/activate" ]]; then
        source ./venv/bin/activate
    elif
        [[ -f "./bin/activate" ]]; then
        source ./bin/activate
    else
        print "Virtual environment not found"
    fi
}

if (( $+commands[gron] )); then
    alias norg="gron --ungron"
    alias ungron="gron --ungron"
fi

# Flatpak aliases
flatpak_path="/var/lib/flatpak/exports/bin"

if [[ -f "${flatpak_path}/org.gnome.Evince" ]]; then
    alias evince="${flatpak_path}/org.gnome.Evince"
fi

if [[ -f "${flatpak_path}/org.gnome.gedit" ]]; then
    alias gedit="${flatpak_path}/org.gnome.gedit"
fi

if [[ -f "${flatpak_path}/org.inkscape.Inkscape" ]]; then
    alias inkscape="${flatpak_path}/org.inkscape.Inkscape"
fi

if [[ -f "${flatpak_path}/org.gnome.Polari" ]]; then
    alias polari="${flatpak_path}/org.gnome.Polari"
fi

if [[ -f "${flatpak_path}/com.visualstudio.code.oss" ]]; then
    alias vscode="${flatpak_path}/com.visualstudio.code.oss"
fi

if [[ -f "${flatpak_path}/org.libreoffice.LibreOffice" ]]; then
    alias loffice="${flatpak_path}/org.libreoffice.LibreOffice"
    alias lowriter="${flatpak_path}/org.libreoffice.LibreOffice --writer"
    alias localc="${flatpak_path}/org.libreoffice.LibreOffice --calc"
    alias loimpress="${flatpak_path}/org.libreoffice.LibreOffice --impress"
fi

if [[ -f "${flatpak_path}/org.gnome.meld" ]]; then
    alias meld="${flatpak_path}/org.gnome.meld"
fi


# load cd dirstack
DIRSTACKSIZE=10
DIRSTACKFILE=~/.zdirs
touch $DIRSTACKFILE
if [[ "$#dirstack" -eq 0 ]]; then
    dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
    [[ -d "$dirstack[1]" ]] && cd $dirstack[1] && cd $OLDPWD
fi


#### User-specific settings
case $(whoami) in
    root)
        # This *may* not be desirable if you routinely need to let
        # root shells sit idle for long periods of time, i.e. if you
        # use Emacs' Tramp package to edit files on remote hosts via
        # an SSH connection.
        export TMOUT=3600
        ;;
esac


## Keybindings
bindkey -e
bindkey '^[OP' where-is # F1 key
bindkey '^[q' push-line-or-edit
bindkey '^[OA' history-beginning-search-backward
bindkey '^[OB' history-beginning-search-forward

## copy/paste between hosts
pull-clipboard-from () {
    if [[ "$@" = "" ]]; then
        print "Error: missing remote host."
        return 1
    fi

    ssh "$@" "DISPLAY=:0 xsel -o" | xsel --clipboard
    print -z $(xsel -o --clipboard)
}
push-clipboard-to () {
    if [[ "$@" = "" ]]; then
        print "Error: missing remote host."
        return 1
    fi

    ssh "$@" "print -n $(xsel -o --clipboard) | DISPLAY=:0 xsel -i --clipboard"
}

emacsclient () {
    /usr/bin/emacsclient -n -e "(if (> (length (frame-list)) 1) 't)" | grep t
    if [[ "$?" = "1" ]]; then
        /usr/bin/emacsclient -c -a "" "$@"
    else
        /usr/bin/emacsclient -a "" "$@"
    fi
}

# update the dirstack
chpwd () {
    local -a dirs; dirs=( "$PWD" ${(f)"$(< $DIRSTACKFILE)"} )
    print -l ${${(u)dirs}[0,$DIRSTACKSIZE]} > $DIRSTACKFILE # ${(u)dirs...} expansion of unique elements
}

# show dots when completing/expanding arguments
_expand-or-complete-with-dots () {
    # toggle line-wrapping off and back on again
    [[ -n "$terminfo[rmam]" && -n "$terminfo[smam]" ]] && echoti rmam
    print -n "\e[31m...\e[0m"
    [[ -n "$terminfo[rmam]" && -n "$terminfo[smam]" ]] && echoti smam
    zle expand-or-complete
    zle redisplay
}
zle -N _expand-or-complete-with-dots
bindkey "^I" _expand-or-complete-with-dots

# cd to parent dir
pcd () {
    cd ${PWD%/$1/*}/$1
}

# clear screen and reset exit code
_clear-screen () {
    BUFFER=' clear'
    zle accept-line
}
zle -N _clear-screen
bindkey "^L" _clear-screen

DEFAULT_USER=$(cat ~/.dotfiles/default_user)

set-prompt () {
    setopt prompt_subst
    typeset -A prc

    # left prompt definitions
    prc[rc]='%(?..%{${fg[red]}%}%B%? %b%{${fg[default]}%})' # return code
    prc[color]='%(!.%{${fg[red]}%}.%{${fg[green]}%})'
    prc[user-host]='$(user-host)'
    prc[promptchar]='%{${fg[default]}%}%(!.#.$)'
    # right prompt definitions
    prc[battery]='$(_battery-level)'
    prc[virtualenv-name]='$(_virtualenv-name)'
    prc[git-info]='$(git-prompt-info)'
    prc[abbrevpath]='%$(($COLUMNS / 2))<...<$(_disambiguate -k)%<<'

    PROMPT="${prc[rc]}${prc[color]}${prc[user-host]}${prc[promptchar]} "
    RPROMPT="${prc[battery]} ${prc[virtualenv-name]}${prc[git-info]}${prc[abbrevpath]}"
    unset prc
}

# check if the shell is connected to a remote machine
# return 1 if false
# argument: PID
is-remote-p() {
    local -a parent

    # if no argument is passed, it checks the parent PID
    [[ "$#" == 0 ]] && 1="$PPID"

    while [[ "$parent[1]" != "1" ]]; do
        parent=($(ps -o ppid=,comm= --pid "$1" 2> /dev/null))

        # on termux init is missing and ps returns error
        if [ $? -ne 0 ]; then
            return 1
        fi

        if [[ "$parent[2]" =~ "sshd|mosh-server" ]]; then
            return 0
        fi

        1=$parent[1]
    done

    return 1
}


# SHELL_HAS_REMOTE_PARENT is evaluated and exported in .zshenv
user-host () {
    local userathost has_remote_parent

    if ( is-remote-p "$PPID" ); then
        has_remote_parent="true"
    elif [[ "$TERM" =~ "screen-256color|screen|tmux" ]]; then
        for i in $(pgrep tmux); do
            ( is-remote-p "$i" ) && has_remote_parent="true"
        done
    fi

    if [[ "$USER" != "$DEFAULT_USER" ]]; then
        userathost="%n "
    fi

    if [[ -n "$has_remote_parent" && -n "$userathost" ]]; then
        userathost="%n@%m "
    elif
        [[ -n "$has_remote_parent" ]]; then
        userathost="@%m "
    fi

    print $userathost
}

_virtualenv-name () {
    if [[ -n "$VIRTUAL_ENV" ]] && print "(%{${fg[yellow]}%}${VIRTUAL_ENV##*/}%{${reset_color}%})"
    #if [[ -n "$VIRTUAL_ENV" ]] && print "(%{${fg[yellow]}%}${FOOBAR##*/}%{${reset_color}%})"
}

git-prompt-info () {
    local gitbranch gitstatus filestat gitinfo

    gitbranch="$(git symbolic-ref --short HEAD 2> /dev/null || git rev-parse --short HEAD 2> /dev/null)"

    if [[ "$gitbranch" ]]; then
        # I know I shouldn't use porcelain, but it's so convinient...
        gitstatus="$(git status -s --porcelain 2> /dev/null)"
        if [[ "$gitstatus" ]]; then
            # staged
            filestat="$(print $gitstatus | rg '^[MA] ' | wc -l)"
            if [[ "$filestat" != "0" ]]; then
                gitinfo+="%{${fg[green]}%}+$filestat%{${reset_color}%}"
            fi
            # unstaged
            filestat="$(print $gitstatus | rg '^ M ' | wc -l)"
            if [[ "$filestat" != "0" ]]; then
                gitinfo+="%{${fg[red]}%}+$filestat%{${reset_color}%}"
            fi
            # untracked
            filestat="$(print $gitstatus | rg '^?? ' | wc -l)"
            if [[ "$filestat" != "0" ]]; then
                gitinfo+="%{${reset_color}%}+$filestat"
            fi
        else
            gitinfo="%{${fg_bold[green]}%}✔"
        fi

        print "%{${fg_bold[blue]}%}@$gitbranch%{${reset_color}%}:$gitinfo%{${reset_color}%} "
    fi
}

_disambiguate () {
    # for compatibility
    setopt localoptions noksharrays

    # this is a modification of Mikachu's disambiguate-keeplast, adding support
    # for an argument to use instead of PWD, and a second argument which is
    # used as a prefix for globbing, but is not part of the disambiguated path.
    # it also supports the -k parameter to keep the last part intact (rather
    # than using a second function for this behavior)

    local short full part cur output
    local first
    local -a split    # the array we loop over

    # need to do it this way
    local treatlast=1
    if [[ "$1" == "-k" ]]; then
        treatlast=
        shift
    fi

    1=${1:-$PWD}
    local prefix="$2"

    if [[ "$1" == "/" ]]; then
        print "/"
        return 0
    fi

    # We do the (D) expansion right here and
    # handle it later if it had any effect
    split=(${(s:/:)${(Q)${(D)1:-$1}}})

    # Handling. Perhaps NOT use (D) above and check after shortening?
    if [[ -z $prefix && $split[1] = \~* ]]; then
        # named directory we skip shortening the first element
        # and manually prepend the first element to the return value
        first=$split[1]
        # full should already contain the first
        # component since we don't start there
        full=$~split[1]
        shift split
    fi

    # we don't want to end up with something like ~/
    if [[ -z "$prefix" ]] && (( $#split > 0 )); then
        part=/
    fi

    # loop over all but the last, plus the last if keeplast is zero
    for cur in $split[1,-2] ${treatlast:+$split[-1]}; {
        while {
            part+=$cur[1]
            cur=$cur[2,-1]
            local -a glob
            glob=( $prefix$full/$part*(-/N) )
            # continue adding if more than one directory matches or
            # the current string is . or ..
            # but stop if there are no more characters to add
            (( $#glob > 1 )) || [[ $part == (.|..) ]] && (( $#cur > 0 ))
        } { # this is a do-while loop
        }
              full+=$part$cur
              short+=$part
              part=/
    }
               # piece them together
               output=$first$short
               # if we skipped the last, add it unaltered
               (( ! treatlast )) && output+=$part$split[-1]
               print $output
               return 0
}

TRAPUSR1 () {
    if [[ -o INTERACTIVE ]]; then
        # print "trapped USR1"
        zle && zle reset-prompt
    fi
}

# update prompt every 60 sec
update-prompt-and-resched () {
    integer i=${"${(@)zsh_scheduled_events#*:*:}"[(I)update-prompt-and-resched]}
    (( i )) && sched -$i
    sched +60 update-prompt-and-resched
    zle && zle reset-prompt
    return 0
}

# continuosly updated prompt is only activated when a battery is found
batteries=( /sys/class/power_supply/BAT*(N) )
if (( $#batteries > 0 )) ; then
    update-prompt-and-resched
fi

# create string with battery information for prompt
_battery-level () {
    local batteries bat capacity color batstatus
    batteries=( /sys/class/power_supply/BAT*(N) )
    if (( $#batteries > 0 )) ; then
        for bat in $batteries ; do
            capacity=$(< $bat/capacity)
            case $(< $bat/status) in
                Charging)
                    batstatus='⚡'
                    color='green'
                    ;;
                Discharging)
                    batstatus=''
                    if (( capacity > 30 )); then
                        color='green'
                    elif (( capacity > 10 )); then
                        color='yellow'
                    else
                        color='red'
                    fi
                    ;;
                Full)
                    batstatus='∞'
                    color='green'
                    ;;
                *) # Unknown
                    batstatus='??'
                    color='yellow'
                    ;;
            esac
            print "%{$fg[$color]%}${batstatus}${capacity}%%%{$reset_color%}"
        done
    fi
}

git-monitor-status () {
    clear
    inotifywait --quiet -mr -e create,delete,modify,move --format "%w %f %e" @./.git . \
        | while read file; do
        clear
        git status --short
        git --no-pager diff --shortstat
    done
}

_monitor-dir-events () {
    inotifywait --quiet -mre create,delete,modify,move --format "%w %f %e" @./.git . 2> /dev/null \
        | while read file; do
        kill -s USR1 $(pidof zsh) 2> /dev/null
    done
}

## handle terminal
case $TERM in
    # screen*)
    # precmd () {
    #     print -Pn "\e]0;%n@%m:%~\a"
    #     #print -Pn "\033k$(whoami)@$(hostname -s)\033\\"
    # }
    # ;;
    (xterm*|screen*|tmux)
        set-prompt
        add-zsh-hook precmd () { [[ $HISTFILE ]] && print -Pn "\e]0;%n@%m:%~\a" || print -Pn "\e]0;%n@%m:%~ no-history\a" }
        # check if running in a gnome-terminal
        # if (( ${+VTE_VERSION} )); then
        #     export TERM="xterm-256color"
        # fi
        ;;
    eterm-color)
        print "EMACS TERMINAL"
        ;;
    dumb)
        unsetopt zle
        PS1='$ '
        HISTFILE='~/.tramp-history'
        PAGER=cat
        alias less=$PAGER
        ;;
esac


# searcg zsh documentation
zman () {
    PAGER="less -g -s '+/^\s{7}'$1" man zshall
}

# quote last word on current line
# _quote-last-word () {
#     emulate -L zsh
#     setopt extendedglob
#     [[ "$LBUFFER" = (#b)(*((#s)|[[:blank:]]))([^[:blank:]]##)([[:blank:]]#) ]] &&
#            {
#                LBUFFER=$match[1]${(qq)match[3]}$match[4]
#                (($#match[4])) || ((CURSOR--))
#            }
#            ((CURSOR++))
#        }
#        zle -N _quote-last-word
#        bindkey "^['" _quote-last-word

# transpose filename with spaces
autoload -Uz transpose-words-match
zstyle ':zle:transpose-words' word-style shell
zle -N transpose-words transpose-words-match

# # quote the last command to reuse output
# backquote-last-command () {
#     BUFFER='$( '"$BUFFER"' )'; CURSOR=0
# }
# zle -N backquote-last-command
# bindkey '^[`' backquote-last-command
_insert-last-command-output () {
    #LBUFFER+="$(eval $history[$((HISTCMD-1))] | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g')"
    LBUFFER+="$(LS_COLORS= TERM=vt220 eval $history[$((HISTCMD-1))])"
}
zle -N _insert-last-command-output
bindkey '^[`' _insert-last-command-output

# bindkey '^R' history-incremental-pattern-search-backward
# bindkey '^S' history-incremental-pattern-search-forward
bindkey '^P' up-line-or-search
bindkey '^N' down-line-or-search

_capitalize-word-before-cursor () {
    zle backward-word
    zle up-case-word
}
zle -N _capitalize-word-before-cursor
bindkey '^[u' _capitalize-word-before-cursor

trigrun () {
    TRIGGER=$1
    command $TRIGGER
    while inotifywait -q -e close_write --format "${fg[blue]}%w${fg[default]} changed" $TRIGGER
    do
        print
        command $TRIGGER
    done
}

# cde - cd to working directory of current emacs buffer
cde () {
    cd ${(Q)~$(emacsclient -e '(with-current-buffer
                               (window-buffer (selected-window))
                               default-directory) ')}
}

# absolute [FILE...] - print PWD/FILE, i.e. filename with fullpath
absolute () {
    print -l ${${@:-$PWD}:a}
}

# total [-F<sep>] [FIELDNUM] - sum up numbers in a column
total () {
    local F
    expr "$1" : -F >/dev/null && F=$1 && shift
    awk $F -v f=${1:-1} '{s+=$f} END{print s}'
}

nfocat () {
    iconv -f cp437 "$@"
}

# up [|N|pat] -- go up 1, N or until basename matches pat many directories
# just output directory when not used interactively, e.g. in backticks
up() {
    local op=print
    [[ -t 1 ]] && op=cd
    case "$1" in
        ('') up 1;;
        (-*|+*) $op ~$1;;
        (<->) $op $(printf '../%.0s' {1..$1});;
        (*) local -a seg; seg=(${(s:/:)PWD%/*})
            local n=${(j:/:)seg[1,(I)$1*]}
            if [[ -n $n ]]; then
                $op /$n
            else
                print -u2 up: could not find prefix $1 in $PWD
                return 1
            fi
    esac
}

zmodload zsh/complist
# local completions
fpath=(~/.dotfiles/zsh_completion $fpath)

autoload -Uz compinit && compinit


zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' special-dirs ..
zstyle ':completion:*' use-ip true
#zstyle ":completion:*" show-completer true
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' rehash true

zstyle ':completion:*' list-colors ''
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*:functions' ignored-patterns '_*'

zstyle ':completion:*:*:*:*:processes' force-list always
zstyle ':completion:*:*:*:*:processes' menu yes select
#zstyle ':completion:*:*:*:*:processes' list-colors "=(#b) #([0-9]#) #([0-9]#:[0-9]#:[0-9]#)*=0=34=36"
zstyle ':completion:*:*:*:*:processes' list-colors "=(#b) #(?# )#(??:??:??)*=0=34=36"
zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,time,args -w"

zstyle ':completion:*:*:cd:*:directory-stack' force-list always
zstyle ':completion:*:*:cd:*:directory-stack' menu yes select
zstyle ':completion:*:*:cd:*:directory-stack' list-colors '=(#b) #([0-9] --)*=34=0'
zstyle ':completion:*:*:cd:*:*' menu select

zstyle ':completion:*:evince::' \
       file-patterns '*.(#i)(dvi|djvu|tiff|pdf|ps|xps)(|.bz2|.gz|.xz|.z) *(-/)' '*'
#compdef pkill=killall
compdef _gnu_generic curl emacs emacsclient file head mv paste
compdef _gnu_generic tail touch shred watch wc zsh

# [ -f ~/.ssh/config ] && : ${(A)ssh_config_hosts:=${${${${(@M)${(f)~~"$(<~/.ssh/config)"}:#Host *}#Host }:#*\**}:#*\?*}} || ssh_config=()
# #[ -f ~/.ssh/known_hosts ] && : ${(A)ssh_known_hosts:=${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[0-9]*}%%\ *}%%,*}} || ssh_known_hosts=()
# [ -r /etc/hosts ] && : ${(A)etc_hosts:=${(s: :)${(ps:\t:)${${(f)~~"$(</etc/hosts)"}%%\#*}##[:blank:]#[^[:blank:]]#}}} || etc_hosts=()
# #[ -x /usr/bin/avahi-browse ] && : ${(A)avahi_hosts::=${(@M)${(s:;:)$(avahi-browse -pkct _workstation._tcp 2>/dev/null)}:#*91*93}} || avahi_hosts=()
# #[ -x /usr/bin/avahi-browse ] && : avahi_hosts=$(avahi-browse -tak | ag workstation| cut -d ' ' -f 5) || avahi_hosts=()
# zstyle ':completion:*:*:(ssh|scp|ping|host|nmanp|rsync):*' hosts $ssh_config_hosts $ssh_known_hosts $etc_hosts

# colored man pages
man() {
    env \
	LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	LESS_TERMCAP_md=$(printf "\e[1;31m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;32m") \
	PAGER="${commands[less]:-$PAGER}" \
	_NROFF_U=1 \
	PATH="$HOME/bin:$PATH" \
	man "$@"
}

bases() {
    print "d:$(([##10]$1)) h:$(([##16]$1)) o:$(([##8]$1)) b:$(([##2]$1))"
}

# QR codes
qr() {
    qrencode "$1" -t ANSIUTF8 -o -
}

# enter alone to list files in current dir or show status in git repo
_my-accept-line() {
    # check if the buffer does not contain any words
    if [[ ${#${(z)BUFFER}} -eq 0 ]]; then
        # put newline so that the output does not start next
        # to the prompt
        print
        # check if inside git repository
        if git rev-parse --git-dir > /dev/null 2>&1 ; then
            # if so, execute `git status'
            git status
        else
            # else run `ls'
            ls --color=auto --classify -ltr
        fi
    fi
    # in any case run the `accept-line' widget
    zle .accept-line
}

zle -N accept-line _my-accept-line
# rebind Enter, usually this is `^M'
bindkey '^M' accept-line

# # don't put a list of commands in history
# zshaddhistory () {
#   emulate -L zsh
#   if ! [[ "$1" =~ "(^ |--password|^rm|^[p]*kill)" ]] ; then
#       print -sr -- "${1%%$'\n'}"
#       fc -p
#   else
#       return 1
#   fi
# }

# Complete from tmux pane.
_tmux_pane_words() {
    local expl
    local -a w
    if [[ -z "$TMUX_PANE" ]]; then
        _message "not running inside tmux!"
        return 1
    fi
    # capture current pane first
    w=( ${(u)=$(tmux capture-pane -J -p)} )
    for i in $(tmux list-panes -F '#P'); do
        # skip current pane (handled above)
        [[ "$TMUX_PANE" = "$i" ]] && continue
        w+=( ${(u)=$(tmux capture-pane -J -p -t $i)} )
    done
    _wanted values expl 'words from current tmux pane' compadd -a w
}

ps4-rpi-install () {
    PS4_IP='192.168.0.10'
    curl -v 'http://'${PS4_IP}':12800/api/install' \
    --data '{"type":"direct","packages":["'${1}'"]}'
}

zle -C tmux-pane-words-prefix   complete-word _generic
bindkey '^Xt' tmux-pane-words-prefix
zle -C tmux-pane-words-anywhere complete-word _generic
bindkey '^Xx' tmux-pane-words-anywhere
#zstyle ':completion:tmux-pane-words-prefix:*' completer _tmux_pane_words
#zstyle ':completion:tmux-pane-words-prefix:*' ignore-line current
zstyle ':completion:tmux-pane-words-(prefix|anywhere):*' completer _tmux_pane_words
zstyle ':completion:tmux-pane-words-(prefix|anywhere):*' ignore-line current
# display the (interactive) menu on first execution of the hotkey
zstyle ':completion:tmux-pane-words-(prefix|anywhere):*' menu yes select interactive
zstyle ':completion:tmux-pane-words-anywhere:*' matcher-list 'b:=* m:{A-Za-z}={a-zA-Z}'

# OPAM configuration
#source /home/${user}/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# virtualenvwrapper
#export WORKON_HOME=~/.virtualenvs
#export PROJECT_HOME=~/Devel

# pyenv + virtualenvwrapper
export VIRTUAL_ENV_DISABLE_PROMPT=yes # virtualenv disabled cause moved to RPROMPT above
