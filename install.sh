#!/bin/sh
# install some dot files in the home
DOTDIR="${HOME}/.dotfiles"

if [ ! -d "$DOTDIR" ]; then
    mkdir "$DOTDIR" && \
        cp -a .git * "$DOTDIR"
fi

for file in \
        fdignore\
        dir_colors \
        tmux.conf \
        zshenv \
        zshrc \
        zile
do
    ln -fs "${DOTDIR}/${file}" "${HOME}/.${file}"
done
